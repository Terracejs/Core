import * as assert from "assert";
import { Mock } from 'typemoq';
import { WorkerRunner, ConfigLoader } from "@Terracejs/Core";
import { Injector } from "injection-js";

describe("WorkerRunner tests", function () {
	describe("Constructing the runner", function () {
		it("Creates a new runner in a child process", function () {
			WorkerRunner.Instance;
		});

		it("Fails in a master process", async function () {
			let Config = Mock.ofType<ConfigLoader>();
			let Inj = Mock.ofType<Injector>();

			assert.throws(function () {
				new WorkerRunner(Config.object, Inj.object, true);
			});
		});

		it("Returns already created instance when created at least once", function () {
			let Config = Mock.ofType<ConfigLoader>();
			let Inj = Mock.ofType<Injector>();

			new WorkerRunner(Config.object, Inj.object, false);
		});
	});
});