import * as assert from "assert";
import * as mocha from 'mocha';
import * as mock from "mock-require";
import ConfigLoader from "../src/ConfigLoader";


describe("ConfigLoader Tests", function () {
	describe("Instance", function () {
		it("Returns an instance of the class", function () {
			assert.equal(true, ConfigLoader.Instance instanceof ConfigLoader);
		});
	});

	describe("getValue", function () {
		it("Errors on invalid path", function () {
			let config = ConfigLoader.Instance;

			assert.throws(function () {
				config["getValue"](1, ["abc", "def"], 0);
			});
		});

		it("Errors if object doesn't have property", function () {
			let config = ConfigLoader.Instance;

			assert.throws(function () {
				let obj = {};
				config["getValue"](obj, ["abc", "def"], 0);
			});
		});

		it("Returns the appropriate value on object", function () {
			let config = ConfigLoader.Instance,
				obj = { testValue: "123" };

			assert.equal(obj.testValue, config["getValue"](obj, ["obj", "testValue"], 1));
		});

		it("Errors if entering an array without number index", function () {
			let config = ConfigLoader.Instance,
				obj = ["123", "456"];

			assert.throws(function () {
				config["getValue"](obj, ["asdf", "asdf"], 1);
			});
		});

		it("Returns the appropriate value on array", function () {
			let config = ConfigLoader.Instance,
				obj = ["123", "456"];

			assert.equal(obj[0], config["getValue"](obj, ["", "0"], 1));
		});
	});

	describe("load", function () {
		let temp = { data: true };
		
		before(function () {
			mock('temp', temp);
		});

		beforeEach(function () {
			ConfigLoader.Instance["_configs"].clear();
		});

		after(function () {
			mock.stop('temp');
		});

		afterEach(function () {
			ConfigLoader.Instance.removeAllListeners();
		});

		it("Loads data from configFile", async function () {
			let config = ConfigLoader.Instance;
			await config.load("temp");

			assert.equal(config["_configs"].get("data"), temp.data);
			assert.equal(config["_loaded"], true);
		});

		it("Emits error on error from configFile", async function () {
			let config = ConfigLoader.Instance;

			config.on("error", function (err) {
				assert(err)
			});

			await config.load("doesn't exist");
		});
	});

	describe("set", function () {
		it("Errors", function () {
			assert.throws(function () {
				ConfigLoader.Instance.set("nope", {});
			});
		});
	});

	describe("Loaded", function () {
		it("returns loaded value", function () {
			assert.equal(ConfigLoader.Instance.Loaded, true);
		});
	});

	describe("get", function () {
		let testConfig = {
			test1: {
				value: 1,
				data: [1,2,3,4,5]
			},
			test2: {
				value: 2,
				test2_1: {
					value: 3,
					data: [5,4,3,2,1]
				}
			}
		};

		before(function () {
			mock('/tmp/test', testConfig);
			ConfigLoader.Instance["_configs"].clear();
			ConfigLoader.Instance.load("/tmp/test");
		});

		it("Returns the appropriate value", function () {
			let config = ConfigLoader.Instance;

			assert.equal(config.get("test1.value"), testConfig.test1.value);
			assert.equal(config.get("test2.test2_1.data"), testConfig.test2.test2_1.data);
		});

		it("Errors on missing object", function () {
			let config = ConfigLoader.Instance;

			assert.throws(function () {
				config.get("doesn't exist");
			});
		});
	});
});