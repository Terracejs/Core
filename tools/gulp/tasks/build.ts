import { task, watch, src, dest, series } from 'gulp';
import * as path from 'path';
import { parallel } from 'gulp';

import { DIST_ROOT, PROJECT_ROOT, SOURCE_ROOT, LICENSE_BANNER } from "../constants";
import { tsBuildTask, copyTask } from "../taskHelpers";

let buildTask = tsBuildTask(SOURCE_ROOT);

let copyAssets = copyTask([
	path.join(DIST_ROOT, '**/*.!(ts)'),
	path.join(PROJECT_ROOT, 'README.md'),
	path.join(PROJECT_ROOT, 'LICENSE')
], DIST_ROOT);

task('build:development', parallel(copyAssets, buildTask));

task('build:production', series('clean', parallel(buildTask, copyAssets)));
