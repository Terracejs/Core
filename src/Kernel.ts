import * as env from "dotenv";
import { EventEmitter } from "events";
import { Injector, ReflectiveInjector, Injectable, InjectionToken, Inject } from "injection-js"
import * as Helpers from "./HelperFunctions";
import ConfigLoader from "./ConfigLoader";
import * as cluster from "cluster";
import { Cluster } from "cluster";
if (!global["Reflect"]) require("reflect-metadata");

let instance: Kernel = null;
let injector: Injector = null;

/**
 * Class for initializing and managing the various
 * processes and services of the application.
 */
@Injectable()
export class Kernel extends EventEmitter {
	private _injector: Injector;
	private _config: ConfigLoader;
	private _cluster: Cluster;
	
	/**
	 * Create a Kernel instance. Kernel is a singleton so only one can be created.
	 */
	constructor(injector: Injector, config: ConfigLoader, @Inject('Cluster') cluster: Cluster) {
		if(cluster.isWorker){
			throw new Error("Must be run in parent process");
		}
		
		if (instance !== null) return instance;
		
		super();

		this._injector = injector;
		this._config = config;
		this._cluster = cluster;
	}

	/**
	 * Get the Kernel Instance
	 * 
	 * @returns {Kernel} Kernel instance
	 */
	public static get Instance() {
		if (instance === null) {
			instance = injector.get(Kernel);
		}

		return instance;
	}
}

injector = ReflectiveInjector.resolveAndCreate([Kernel, ConfigLoader, {provide: 'Cluster', useValue: cluster}]);

export default Kernel;