export interface IWorker {
	Start();
	Stop();
	Pause();
	Initialize(): Promise<boolean>;
	Status(): WorkerStatus;
}

export interface IWorkerConstructor {
	new (settings: object): IWorker;
}

export enum WorkerStatus {
	Running,
	Paused,
	Stopped,
	Finished
}