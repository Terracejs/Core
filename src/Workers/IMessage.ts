export interface IMessage {
	type: string;
	data: object;
	worker: string;
}