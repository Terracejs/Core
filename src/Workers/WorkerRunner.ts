import { IWorker, IWorkerConstructor, WorkerStatus } from "./IWorker";
import { IMessage } from "./IMessage";
import { Injector, ReflectiveInjector, Injectable } from "injection-js";
import * as Helpers from "../HelperFunctions";
import ConfigLoader from "../ConfigLoader";

if (!global["Reflect"]) require("reflect-metadata");

let workerCache: Map<string, IWorkerConstructor> = new Map();
let injector: Injector = null;
let instance: WorkerRunner = null;

/**
 * Class to manage workers in a child process.
 */
@Injectable()
export class WorkerRunner {
	// Map of workers
	private _workers: Map<string, IWorker>;
	private _injector: Injector;
	private _config: ConfigLoader;

	/**
	 * Create a new runner.
	 */
	constructor(config: ConfigLoader, injector: Injector, isMaster: boolean) {
		if (isMaster) {
			throw new Error("Must be run in a child process");
		}

		if (instance !== null) return instance;

		this._workers = new Map();
		this._config = config;
		this._injector = injector;
		this.setupCommunication();
	}

	/**
	 * Create a new worker and initialize it.
	 * 
	 * @param workerData {object} Information on creating a worker.
	 */
	async newWorker(workerData) {
		if (!workerCache.has(workerData.workerFile)) {
			let constructor = await import(workerData.workerFile);
			workerCache.set(workerData.workerFile, constructor);
		}

		let worker: IWorker = new (workerCache.get(workerData.workerFile))(workerData.settings);
		let result = await worker.Initialize();
	}

	/**
	 * Start a worker
	 * 
	 * @param worker {string} Worker name.
	 */
	startWorker(worker: string) {
		if (this._workers.has(worker)) {
			this._workers.get(worker).Start();
		}
	}

	/**
	 * Stop a worker
	 * 
	 * @param worker {string} Worker name.
	 */
	stopWorker(worker: string) {
		if (this._workers.has(worker)) {
			this._workers.get(worker).Stop();
		}
	}

	/**
	 * Pause a worker
	 * 
	 * @param worker {string} Worker name.
	 */
	pauseWorker(worker: string) {
		if (this._workers.has(worker)) {
			this._workers.get(worker).Pause();
		}
	}

	/**
	 * Get the status for a worker
	 * 
	 * @param worker {string} Worker name.
	 * @returns {WorkerStatus} The status of the worker
	 */
	status(worker: string): WorkerStatus {
		if (this._workers.has(worker)) {
			return this._workers.get(worker).Status();
		}
	}

	/**
	 * Setup message handler from parent
	 */
	private setupCommunication() {
		process.on('message', async (message: IMessage) => {
			switch (message.type) {
				case 'new_worker':
					await this.newWorker(message.data);
					break;
				case 'start_worker':
					this.startWorker(message.worker);
					break;
				case 'stop_worker':
					this.stopWorker(message.worker);
					break;
				case 'pause_worker':
					this.pauseWorker(message.worker);
					break;
				case 'status_worker':
					var stat = this.status(message.worker);
					// TODO: Send status to parent
					break;
				case 'status':
					var status = {
						names: this._workers.keys(),
						statuses: new Map()
					};

					for (let [name, worker] of this._workers) {
						status.statuses.set(name, worker.Status);
					}

					// TODO: Return status to parent
					break;
				case 'start':
					for (let worker of this._workers.values()) {
						worker.Start();
					}

					break;
				case 'pause':
					for (let worker of this._workers.values()) {
						worker.Pause();
					}
					break;
				case 'stop':
					for (let worker of this._workers.values()) {
						worker.Stop();
					}
					break;
				default:
			}
		});
	}

	public static get Instance() {
		if (instance === null) {
			instance = injector.get(WorkerRunner);
		}

		return instance;
	}
}

export interface RunnerStatus {
	workers: Array<string>,
	statuses: Map<string, WorkerStatus>
}

injector = ReflectiveInjector.resolveAndCreate([
	ConfigLoader,
	WorkerRunner,
	{provide: 'isMaster', useValue: require("cluster").isMaster}
]);