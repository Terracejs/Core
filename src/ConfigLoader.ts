import { EventEmitter } from "events";
import { Injectable } from "injection-js";

// The config loader is a singleton.
// We store the ref to it here.
let instance: ConfigLoader = null;

/**
 * Class for loading configs into the
 * system. This class is a singleton.
 * 
 * @fires ConfigLoader#loaded
 * @fires ConfigLoader#error
 */
@Injectable()
export class ConfigLoader extends EventEmitter {
	/**
	 * Map of config options
	 */
	private _configs: Map<string, any>;

	/**
	 * Whether the loader is done loading
	 */
	private _loaded: boolean;

	// TODO: Build file generator/reader thingy
	constructor() {
		// Return ref it exists
		if (instance !== null) return instance;

		super();

		this._configs = new Map<string, any>();
		this._loaded = false;
	}

	/**
	 * Get the value at the end of path.
	 * 
	 * @param obj {any} The object to get value from
	 * @param path {Array<string>} The namespace path to the object
	 * @param index {number} The current index in the namespace
	 */
	private getValue(obj: any, path: Array<string>, index: number): any {
		if (index === path.length) {
			return obj;
		} else {

			if (obj instanceof Object) {

				if (obj instanceof Array) {
					let tempIndex = Number.parseInt(path[index]);

					if (!Number.isNaN(tempIndex) && obj.length > tempIndex) {

						return this.getValue(obj[tempIndex], path, index + 1);

					} else {
						throw new TypeError(`Index must be an integer: ${index}`);
					}

				} else if (obj.hasOwnProperty(path[index])) {

					return this.getValue(obj[path[index]], path, index + 1);

				} else {
					throw new TypeError(`Path can't be followed: ${path.join(".")}`);
				}

			} else {
				throw new TypeError(`Path can't be followed: ${path.join(".")}`);
			}

		}
	}

	/**
	 * Load the configuration files
	 * 
	 * @fires ConfigLoader#loaded
	 * @fires ConfigLoader#error
	 * @returns {Promise<void>} Empty Promise
	 */
	public async load(configFile: string): Promise<void> {
		try {
			// Load the config data
			let configData = require(configFile);


			for (let key in configData) {
				this._configs.set(key, configData[key]);
			}

			this._loaded = true;
			this.emit("loaded");
		} catch (err) {
			this.emit("error", err);
		}
	}

	/**
	 * Get the config value
	 * 
	 * @param name {string} Namespace of value to get
	 * @param def {any} default value
	 * 
	 * @throws {ReferenceError}
	 */
	public get(name: string, def: any = null): any {
		let namespace = name.split('.'),
			obj = namespace[0];

		if (def === null && !this._configs.has(obj)) {
			throw new ReferenceError(`${name} doesn't exist`);
		}

		return this.getValue(this._configs.get(obj), namespace, 1);
	}

	public set(name: string, value: any) {
		throw new Error("Not yet Implemented");
	}

	// Whether the config has loaded the data
	public get Loaded(): boolean {
		return this._loaded;
	}

	// Static reference to the loader
	public static get Instance() {
		if (instance === null) {
			instance = new ConfigLoader();
		}

		return instance;
	}
}

export default ConfigLoader;