import { Injectable, Inject } from "injection-js";
import { Observable } from "rxjs";
import * as fs from "fs";
import { resolve } from "path";
import { promisify } from "..";

export type FileSystem = typeof fs;

/**
 * An object to represent a file
 */
@Injectable()
export class File {
	private _contents: Buffer;
	private _path: string;
	private _fd: number;

	constructor(
		@Inject('FileSystem') private fileSystem: FileSystem,
		path: string = '',
		private _encoding: string = 'utf8'
	) {
		this.path = path;
	}

	get path() {
		return this._path;
	}

	set path(path: string) {
		this._path = resolve(path);
	}
}