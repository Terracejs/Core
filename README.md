[![coverage report](https://gitlab.com/Terracejs/Core/badges/master/coverage.svg)](https://gitlab.com/Terracejs/Core/commits/master)
[![pipeline status](https://gitlab.com/Terracejs/Core/badges/master/pipeline.svg)](https://gitlab.com/Terracejs/Core/commits/master)
The Core package for the Terracejs framework.